"use strict";

function createListFromArray(arr, parent = document.body) {
  const ul = document.createElement('ul'); 

  arr.forEach(item => {
    const li = document.createElement('li'); 
    li.textContent = item; 
    
    ul.appendChild(li); 
  });
  
  parent.appendChild(ul); 
}

createListFromArray([1, 22, 333, "Hallo", "world"]);
createListFromArray([`TODAY is ${new Date().getFullYear()}.0${new Date().getMonth()+1}.${new Date().getDate()}` , `TIME is ${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}`])
